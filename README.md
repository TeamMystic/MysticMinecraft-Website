Mystic Minecrsft: Website
=========================

This is the publicly-viewable site for Mystic players that may wish to play or currently play on our
Minecraft server. It exists to serve as a guide of sorts, and to provide documentation on the lesser-known
features and issues related to our modpack.

Licensing
---------

The Mystic Minecraft website is licensed under the MIT license. You may read up on the license in the
[LICENSE file](https://gitlab.com/TeamMystic/MysticMinecraft-Website/blob/master/LICENSE), or at
[choosealicense.com](http://choosealicense.com/licenses/mit/).

Resources used
--------------

* [Bulma](http://bulma.io/): A modern CSS framework based on Flexbox
* [Font-awesome](http://fontawesome.io/): The iconic font and CSS toolkit
* [IcoMoon](https://icomoon.io/): Pixel-perfect icon solutions
* [InstantClick](http://instantclick.io/): Speeds up navigation by minimizing page reloads
